const fs = require('fs');

const JpdfParse = require('pdf2json');

const pdf = './b1.pdf';
let dataBuffer = fs.readFileSync(pdf);

function conv() {
  JpdfParse(dataBuffer).then(function (data) {
    // console.log(Object.keys(data));
    // number of pages
    // console.log(data.numpages);
    // number of rendered pages
    // console.log(data.numrender);
    // PDF info
    // console.log(data.info);
    // PDF metadata
    // console.log(data.metadata);
    // PDF.js version
    // check https://mozilla.github.io/pdf.js/getting_started/
    // console.log(data.version);
    // PDF text
    console.log(data);
  });
}

conv();
