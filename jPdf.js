let fs = require('fs'),
  PDFParser = require('j-pdfjson');

let pdfParser = new PDFParser();

pdfParser.on('pdfParser_dataError', (errData) =>
  console.error(errData.parserError)
);
pdfParser.on('pdfParser_dataReady', (pdfData) => {
  // fs.writeFile(
  //   './pdf2json/test/F1040EZ.fields.json',
  //   JSON.stringify(pdfParser.getAllFieldsTypes())
  // );
  console.log(pdfData);
});

pdfParser.loadPDF('./b1.pdf');
